package main

import ( "net/http"
		 "log"
		 "io/ioutil"
		 "encoding/json"
		 "github.com/gorilla/mux"
		 "time"
		 "strconv"
		 "sync"
		)


	type User struct {
		Email  	  string `json:email`
		Pass   	  string `json:pass` 
		Birthdate string `json:birthdate`
	}

	var registeredUser []User = []User{}

	var messageHeader string = `{
		"message" : "invalid header"
	}`

	var messageBody string = `{
		"message" : "invalid body"
	}`

	var messageSuccessfullSignup string = `{
		"message" : "user succesfully registered"
	}`

	func main() {

		var wg sync.WaitGroup
		wg.Add(1)
		go func() {
			StartServer()
			
			wg.Done()
		}() 
		log.Println("[INFO] Servidor no ar!")
		
		wg.Wait()

			

	}

	func StartServer() {

		duration, _ := time.ParseDuration("1000ns")

		r := mux.NewRouter()

		r.HandleFunc("/SignUp", SignUp)
		r.HandleFunc("/SignIn", SignIn)

		server := &http.Server{
				Addr       : "192.168.0.32:8082",
				IdleTimeout: duration,
				Handler    : r, 
		}

		log.Print(server.ListenAndServe())
	}

	func SignUp(res http.ResponseWriter, req *http.Request) {

		var u User
		body, _ := ioutil.ReadAll(req.Body)
		json.Unmarshal(body, &u)

		if(VerifyHeaderMethod(res, req)){
			if (u.Email == "" || u.Pass == "" || u.Birthdate == ""){
				res.Header().Set("Status", "400")
				res.Write([]byte(messageBody))
			}else{
				registeredUser = append(registeredUser, u)
				res.Header().Set("Status", "200")
				res.Write([]byte(messageSuccessfullSignup))
			}
		}
	}

	func SignIn(res http.ResponseWriter, req *http.Request){

		var u User
		body, _ := ioutil.ReadAll(req.Body)
		json.Unmarshal(body, &u)
		userFound, usuario := FindUser(registeredUser, u)

		if(VerifyHeaderMethod(res, req)){
			if (u.Email == "" ||u.Pass == ""){
				res.Header().Set("Status", "400")
				res.Write([]byte(messageBody))

			}else if (!userFound){
				res.Header().Set("Status", "403")

			}else if (userFound){
				age := AgeVerification(usuario.Birthdate)
				res.Write([]byte(
					`{
		"message" : "user succesfully logon",
		"age" : ` + age + `
	}`))
			}
		}
	}

	func FindUser(registeredUser []User,u User) (bool, User){
		for i := range registeredUser {
		    if (registeredUser[i].Email ==u.Email && registeredUser[i].Pass ==u.Pass) {
				return true, registeredUser[i];
			}
		}
		return false, u;
	}

	func AgeVerification(birthdate string) (string){
		date := time.Now().String()
		year, _ := strconv.Atoi(date[0:4])
		birthyear, _ := strconv.Atoi(birthdate[6:10])
		age := year - birthyear
		return strconv.Itoa(age)
	}

	func VerifyHeaderMethod(res http.ResponseWriter, req *http.Request)(bool){
		usedmethod := req.Method 
		headers := req.Header
		expectedHeader := headers["Content-Type"]

		if (usedmethod != "POST"){
			res.Header().Set("Status", "405")
			return false
		}else if expectedHeader[0] != "application/json"{
			res.Header().Set("Status", "400")
			res.Write([]byte(messageHeader))
			return false
		}
		return true
	}
